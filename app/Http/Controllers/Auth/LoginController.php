<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Contracts\UserRepositoryInterface;
use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;
use Auth;

class LoginController extends Controller
{
    public function __construct(UserRepositoryInterface $user)
    {
        $this->user = $user;
    }   
    public function getLogin()
    {
        return view('auth.login');
    }

    public function postLogin(LoginRequest $request)
    {
        $params = $request->all();
        
        $user = $this->user->login($params);

        if ( $user['status'] ) {
            return redirect('/blog');
        } else {
            return redirect()->back()->with('error_message', 'Email/Password does not match!');
        }
    }

    public function logout()
    {
        Auth::logout();

        return redirect('/login');
    }

}
