<?php

namespace App\Repositories;
use App\Contracts\BlogRepositoryInterface;
use App\Blog;
use Auth;
use Hash;

class BlogRepository implements BlogRepositoryInterface
{   
    public function __construct(Blog $model)
    {
        $this->model = $model;
    }//end __construct

    public function create($params)
    {
        $blog = $this->model->create($params);

        return $blog;
    } 

    public function all()
    {
        $blogs = $this->model->all();

        return $blogs;
    }

    public function show($id)
    {
        $blog = $this->model->find($id);

        return $blog;
    }

    public function update($params, $id)
    {
        $blog = $this->model->find($id)->update($params);

        return $blog;
    }

    public function destroy($id)
    {
        $blog = $this->model->find($id)->delete();

        return $blog;
    }

}//end
