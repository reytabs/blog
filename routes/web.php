<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if(Auth::check())
        return redirect('/blog');
    return redirect('/login');
});

Route::get('register', 'Auth\RegisterController@getRegister');
Route::post('register', 'Auth\RegisterController@postRegister')->name('register');

Route::get('login', 'Auth\LoginController@getLogin');
Route::post('login', 'Auth\LoginController@postLogin')->name('login');

Route::post('logout', 'Auth\LoginController@logout');

Route::group(['middleware' => ['authenticated']], function () {
    Route::get('/blog', 'BlogController@index')->name('home');
    Route::get('/blog/create', 'BlogController@create');
    Route::post('/blog/create', 'BlogController@store');
    Route::get('/blog/edit/{id}', 'BlogController@show');
    Route::post('/blog/update/{id}', 'BlogController@update');
    Route::get('/blog/delete/{id}', 'BlogController@destroy');
});

