<?php

namespace App\Contracts;

interface BlogRepositoryInterface
{
    
    public function create($params);
    public function all();
    public function show($id);
    public function update($params, $id);
    public function destroy($id);

}//end BranchRepositoryInterface
