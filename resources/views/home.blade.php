@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a type="button" href="{{ url('blog/create') }}" class="btn btn-primary">Create</a>
                </div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if(session()->has('success_message'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success_message') }}
                        </div>
                    @endif

                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Title</th>
                                <th scope="col">Description</th>
                                <th scope="col">Edit</th>
                                <th scope="col">Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach( $blogs as $blog )
                                <tr>
                                    <th scope="row">{{ $blog->id }}</th>
                                        <td><a href="{{ url('/blog/edit/'. $blog->id) }}">{{ $blog->title }}</a></td>
                                        <td>{{ $blog->description }}</td>
                                        <td>
                                            <a type="button" href="{{ url('/blog/edit/'. $blog->id) }}" class="btn btn-sm btn-primary">Edit</a>
                                        </td>
                                        <td>
                                            <a type="button" href="{{ url('/blog/delete/'. $blog->id) }}" class="btn btn-sm btn-danger">Delete</a>
                                        </td>
                                    </tr>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
