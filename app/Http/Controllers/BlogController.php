<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\BlogCreateRequest;
use App\Contracts\BlogRepositoryInterface;

class BlogController extends Controller
{

    public function __construct(BlogRepositoryInterface $blog)
    {
        $this->blog = $blog;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blogs = $this->blog->all();

        return view('home', compact('blogs'));
    }   

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('blog.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BlogCreateRequest $request)
    {
        $params = $request->all();

        $blog = $this->blog->create($params);

        if ( $blog )
            return redirect()->back()->with('success_message', 'Blog successfully created!');
            
        return redirect()->back()->with('error_message', 'Unable to create blog!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $blog = $this->blog->show($id);

        return view('blog.details', compact('blog'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BlogCreateRequest $request, $id)
    {
        $params = $request->all();

        $blog = $this->blog->update($params, $id);

        if($blog)
            return redirect('/blog')->with('success_message', 'Blog successfully updated!');

        return redirect()->back()->with('error_message', 'Unable to update blog');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $blog = $this->blog->destroy($id);

        return redirect('/blog');
    }
}
