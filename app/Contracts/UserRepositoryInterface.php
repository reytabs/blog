<?php

namespace App\Contracts;

interface UserRepositoryInterface
{
    
    public function login($params);
    public function register($params);

}//end BranchRepositoryInterface
