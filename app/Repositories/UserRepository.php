<?php

namespace App\Repositories;
use App\Contracts\UserRepositoryInterface;
use App\User;
use Auth;
use Hash;

class UserRepository implements UserRepositoryInterface
{   
    public function __construct(User $model)
    {
        $this->model = $model;
    }//end __construct

    // User login
    public function login($params)
    {
        if (Auth::attempt(['email' => $params['email'], 'password' => $params['password']])) {
            // Authentication passed...
            $response = array(
                'status' => true
            );
        } else {
            $response = array(
                'status' => false
            );
        }
        return $response;
    }
    
    // User registration
    public function register($params)
    {
        $params['password'] = Hash::make($params['password']);

        $register = $this->model->create($params);
        
        return $register;
    }//end register

}//end 
