<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use App\Contracts\UserRepositoryInterface;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    public function __construct(UserRepositoryInterface $user)
    {
        $this->user = $user;
    }
    public function getRegister()
    {
        return view('auth.register');
    }

    public function postRegister(Request $request)
    {
        $params = $request->all();

        $register = $this->user->register($params);

        if($register)
            return redirect('/login');
            
        return redirect()->back()->with('error_message', 'Unable to create user!');
    
    }
}
